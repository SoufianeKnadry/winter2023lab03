import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[]args){
		
		Scanner scan = new Scanner(System.in);
		//Make array
		Microwave[] myMicrowaves = new Microwave[4];
		//Populate array
		for (int i =0;i<myMicrowaves.length;i++){
			
			myMicrowaves[i] = new Microwave();
			System.out.println("Enter the brand");
			myMicrowaves[i].brand = scan.next();
			System.out.println("Enter the color");
			myMicrowaves[i].color= scan.next();
			System.out.println("Enter max duration");
			myMicrowaves[i].maxDuration = scan.nextInt();
		}
		//Print fields of last microwave
		System.out.println(myMicrowaves[3].brand);
		System.out.println(myMicrowaves[3].color);
		System.out.println(myMicrowaves[3].maxDuration);
		
		
		//Calls methods with the first microwave
		myMicrowaves[0].printBrand();
		myMicrowaves[0].heatFood();

	}
}