

public class Microwave{

	public String brand;
	public String color;
	public int maxDuration;

	public void printBrand(){
		System.out.println(this.brand);
	}
	
	public void heatFood(){	
		
		for (int i=this.maxDuration;i>0;i--){
			System.out.println("Your food is ready in "+i+" seconds" );
		}
		System.out.println("YOUR FOOD IS READY!");
	}

}